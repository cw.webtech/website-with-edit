import React from 'react'

export const HomePageTemplate = ({ title = '', posts = [] }) => {
  return (
    <section>
      <h2>{title}</h2>
    </section>
  );
};

const HomePage = ({ data }) => {
  const content = data.page.frontmatter;

  return (
    <HomePageTemplate
      title={content.title}
    />
  );
}

export default HomePage

export const aboutPageQuery = graphql`
  query HomePage($id: String!) {
    page: markdownRemark(id: { eq: $id }) {
      frontmatter {
        title
      }
    }
  }
`
